# README #


### What is this repository for? ###
We are trying to answer the following question.
Given a highly occluded object in a scene, how can we estimate the optimal camera position and view that maximize detection or pickability.

### External and internal views ###
YouTube link: https://www.youtube.com/watch?v=49Nq9TZtd2k

### Iterative Kernel Density Estimation ###
![Screen Shot 2015-06-10 at 2.45.23 pm.png =300x300](https://bitbucket.org/repo/jrAgAz/images/1111521336-Screen%20Shot%202015-06-10%20at%202.45.43%20pm.png =300x300)
### Heatmap and camera position plot ###
![Screen Shot 2015-06-10 at 2.45.23 pm.png =300x300](https://bitbucket.org/repo/jrAgAz/images/1684193019-Screen%20Shot%202015-06-10%20at%202.45.23%20pm.png =300x300)