%Copyright (C) 2015, by Inkyu Sa, enddl22@gmail.com

%This is free software: you can redistribute it and/or modify
%it under the terms of the GNU Lesser General Public License as published by
%the Free Software Foundation, either version 3 of the License, or
%(at your option) any later version.
 
%This software package is distributed in the hope that it will be useful,
%but WITHOUT ANY WARRANTY; without even the implied warranty of
%MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%GNU Lesser General Public License for more details.

%You should have received a copy of the GNU Leser General Public License.
%If not, see <http://www.gnu.org/licenses/>.

function stop = outfun(x,optimValues,state)
    global x_buf;
    %global g_cnt_out;
    global g_prev_sol;
    global g_stop_cnt;
    global g_prev_f;
%     vrep=evalin('base','vrep');
%     endeffectorhandle=evalin('base','endeffectorhandle');
%     clientID=evalin('base','clientID');
    
       
    %x_buf=evalin('base','x_buf');
    %fprintf('outputfun call\n');
    %g_cnt_out=g_cnt_out+1;
    if g_stop_cnt>=20
        stop=true;
    else
        stop = false; 
    end
    if strcmp(state,'iter')
        %sendPositionCmd(clientID,vrep,x'-g_init_pose);
        %[errorCode,position]=vrep.simxGetObjectPosition(clientID,endeffectorhandle,-1,vrep.simx_opmode_buffer);
        x_buf=[x_buf;x];
        disp([' x = ',num2str(x)]);
        g_prev_sol=x;
        if g_prev_f==optimValues.fval
            g_stop_cnt=g_stop_cnt+1;
        else
            g_prev_f=optimValues.fval;
            g_stop_cnt=0;
        end      
    end
%     cur_dist_to_sol=sqrt( (g_prev_sol(1)-x(1))^2+ ...
%             (g_prev_sol(2)-x(2))^2+...
%             (g_prev_sol(3)-x(3))^2     );
   
    g_stop_cnt    
    


end

