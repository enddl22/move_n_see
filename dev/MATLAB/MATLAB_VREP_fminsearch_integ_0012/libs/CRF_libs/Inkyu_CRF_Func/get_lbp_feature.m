function out=get_lbp_feature(im,szPatch,mode,r,nSP)
offset_u=floor(szPatch/2);
offset_v=floor(szPatch/2);
[lv,lu]=size(im);
%out=zeros(lv-offset_v-1,lu-offset_u-1,16); %for raotation variant lbp
out=zeros(lv-offset_v-1,lu-offset_u-1,6); %for rotation invariant lbp
%cnt=1;
if(strcmp(mode,'ri'))
mapping=lbp_getmapping(4,'ri');
end
if nSP==8
    SP=[-r -r; -r 0; -r r; 0 -r; -0 r; r -r; r 0; r r];
else
    SP=[-r 0; 0 -r; -0 r; r 0];
end

for j=offset_v+1:lv-offset_v-1
    for i=offset_u+1:lu-offset_u-1
        patch = im(j-offset_v:j+offset_v,i-offset_u:i+offset_u);
        %lbp(patch,SP,mapping,'nh');
        if(strcmp(mode,'ri'))
            out(j,i,:)=lbp(patch,1,4,mapping,'nh');
        
        else
            out(j,i,:)=lbp(patch,SP,0,'nh');
        end
        %cnt=cnt+1;
    end
end

end