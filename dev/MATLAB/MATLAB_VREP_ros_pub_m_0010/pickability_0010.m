% Copyright (C) 2015, by Inkyu Sa, enddl22@gmail.com
%
% This is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This software package is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Leser General Public License.
% If not, see <http://www.gnu.org/licenses/>.
clear all;
close all;
addpath(genpath('../MATLAB_VREP_ros_pub_m_0010'));
vrep=remApi('remoteApi'); % using the prototype file (remoteApiProto.m)
vrep.simxFinish(-1); % just in case, close all opened connections
clientID=vrep.simxStart('127.0.0.1',19997,true,true,5000,5);
camhandle=[];
endeffectorhandle=[];
endeffectorTrj=[];
imgs={};
init_pos=[-0.9852,1.0174,1.2453];
img_cnt=1;


rosinit
%endPose=zeros(7,1);
detectionMsg=rosmessage('std_msgs/UInt64');
detectionImgMsg=rosmessage('sensor_msgs/Image');
detectionImgMsg.Encoding='mono8';
%MeasurePub = rospublisher('/MATLAB/detector',rostype.std_msgs_UInt64);
MeasurePub = rospublisher('/MATLAB/detector','std_msgs/UInt64');
ImgPub = rospublisher('/MATLAB/image','sensor_msgs/Image');

nvals  = 2;
rez    = .4; % how much to reduce resolution
rho    = .5; % (1 = loopy belief propagation) (.5 = tree-reweighted belief propagation)
feat_params = {{'hsvpatches',0},{'fourier',1},{'hog',32},{'lbp'}};
load('p.mat');
load('thresh.mat');
trained_model=p;
detection_out={};
detec_buf=[];
detection_cnt=1;

draw_detection=1;
draw_img=0;
draw_trj=1;
draw_decMap=1;

if draw_trj
    hFig_Trj=figure('Name','Camera Trajectory');
    hTrj=axes;
end
if draw_img
    hFig_Img=figure('Name','RGB Image');
    hImg=axes;
end

if draw_detection
    hFig_Detec=figure('Name','Pixel Segmentation(prediction)');
    hDetectionImg=axes;
end

if draw_decMap
    hFig_DecMap=figure('Name','Detection Map');
    hDetectionMap=axes;
end


try
	if (clientID>-1)
		disp('Connected to remote API server');
        disp('Starting simulation');
        
        [res,objs]=vrep.simxGetObjects(clientID,vrep.sim_handle_all,vrep.simx_opmode_oneshot_wait);
        if (res==vrep.simx_error_noerror)
            [err,camhandle]=vrep.simxGetObjectHandle(clientID,'Baxter_leftArm_camera',vrep.simx_opmode_oneshot_wait);
            [err,endeffectorhandle]= vrep.simxGetObjectHandle(clientID,'Baxter_leftArm_tip',vrep.simx_opmode_oneshot_wait);
        else
            fprintf('Remote API function call returned with error code: %d\n',res);
        end
    
        vrep.simxStartSimulation(clientID,vrep.simx_opmode_oneshot_wait);
        pause(2);

        sendPositionCmd(clientID,vrep,init_pos);
        pause(13);   
        %This way is a bit dodge but no way to do without information
        %of completion of manipulating.
        %Baxter model implemented in VREP does not provide
        %acknowledgement of manipulating completion.
        %Therefore, just waiting for reasonable time until the arm
        %reaches to the goal.
        
        %endposeSub=rossubscriber('/vrep/LeftEndEffectorPose',@endEffectorPoseCallback);
        %Don't know why the first call always return 0,0,0
        [errorCode,position]=vrep.simxGetObjectPosition(clientID,endeffectorhandle,-1,vrep.simx_opmode_streaming);
        endeffectorTrj = [endeffectorTrj;init_pos];
        if draw_trj
            set(0,'CurrentFigure',hFig_Trj);
            drawCamWithFrustum(hTrj,init_pos(1),init_pos(2),init_pos(3),0,0,0);
            view(-44,22);
        end
        [errorCode,resolution,img]=vrep.simxGetVisionSensorImage2(clientID,camhandle,0,vrep.simx_opmode_oneshot_wait);
        if draw_img
            set(0,'CurrentFigure',hFig_Img);
            imshow(img,'parent',hImg);
        end
        %pause(1)
        imgs{img_cnt}=img;
        img_cnt=img_cnt+1;
        
        [likelihood,prediction,nDetection]=detection(double(img));
        detection_out{detection_cnt}={likelihood,prediction,nDetection};
        detection_cnt=detection_cnt+1;
        detec_buf=[detec_buf;nDetection];
        %drawDetectionMap(detection_out{detection_cnt}.);
        
        %Publish ros message
        detectionMsg.Data=nDetection;
        send(MeasurePub,detectionMsg);
        
        writeImage(detectionImgMsg,uint8(prediction*255));
        send(ImgPub,detectionImgMsg);
        
        if draw_detection
            set(0,'CurrentFigure',hFig_Detec);
            imshow(prediction,'parent',hDetectionImg);
        end
        %Create trajectory
        trj={};
        for i=1:6
            T=transl(0,0,i*0.1);
            p=mkgrid(3,0.1,T); %Arg1=how many points, Arg2=width if 1 then (-0.5~0.5)
            trj{i}=p;
        end

        for j=1:size(trj,2)
            for i=1:size(trj{1,1},2)
                sendPositionCmd(clientID,vrep,trj{1,j}(:,i));
                [errorCode,position]=vrep.simxGetObjectPosition(clientID,endeffectorhandle,-1,vrep.simx_opmode_buffer);
                endeffectorTrj = [endeffectorTrj;position];
                if draw_trj
                    set(0,'CurrentFigure',hFig_Trj);
                    for r=1:size(endeffectorTrj,1)
                        drawCamWithFrustum(hTrj,endeffectorTrj(r,1),endeffectorTrj(r,2),endeffectorTrj(r,3),0,0,0);
                        view(-44,22);
                    end
                    %hold on;
                end 
                [errorCode,resolution,img]=vrep.simxGetVisionSensorImage2(clientID,camhandle,0,vrep.simx_opmode_oneshot_wait);
                imgs{img_cnt}=img;
                img_cnt=img_cnt+1;
                if draw_img
                    set(0,'CurrentFigure',hFig_Img);
                    imshow(img,'parent',hImg);
                end
                
                [likelihood,prediction,nDetection]=detection(double(img));
                detection_out{detection_cnt}={likelihood,prediction,nDetection};
                detection_cnt=detection_cnt+1;
                detec_buf=[detec_buf;nDetection];
                
                %Publish ros message
                detectionMsg.Data=nDetection;
                send(MeasurePub,detectionMsg);
                writeImage(detectionImgMsg,uint8(prediction*255));
                send(ImgPub,detectionImgMsg);
        
                if draw_decMap
                    set(0,'CurrentFigure',hFig_DecMap);
                    drawDetectionMap(hDetectionMap,endeffectorTrj(:,1),endeffectorTrj(:,2),endeffectorTrj(:,3),detec_buf);
                end
                %hold on;
                if draw_detection
                    set(0,'CurrentFigure',hFig_Detec);
                    imshow(prediction,'parent',hDetectionImg);
                end
        
                %pause(0.5)
            end
        end

        for i=1:length(detection_out)
            endeffectorTrj(i,4)=detection_out{1,i}{1,3};
        end
        
        returnCode=vrep.simxStopSimulation(clientID,vrep.simx_opmode_oneshot_wait);
        pause(0.5);
        vrep.simxFinish(clientID); % close the line if still open
        pause(0.5);
	else
		disp('Failed connecting to remote API server');
	end
	vrep.delete(); % call the destructor!
catch err
    returnCode=vrep.simxStopSimulation(clientID,vrep.simx_opmode_oneshot_wait);
	vrep.simxFinish(clientID); % close the line if still open
	vrep.delete(); % call the destructor!
    rosshutdown
end;

disp('Program ended');
