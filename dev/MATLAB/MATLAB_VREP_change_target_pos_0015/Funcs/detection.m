% Copyright (C) 2015, by Inkyu Sa, enddl22@gmail.com
%
% This is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This software package is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Leser General Public License.
% If not, see <http://www.gnu.org/licenses/>.

function [ likelihood,prediction,nDetection ] = detection( img )
    nvals = evalin('base','nvals');
    rez = evalin('base','rez');
    rho = evalin('base','rho');
    feat_params = evalin('base','feat_params');
    trained_model=evalin('base','trained_model');
    thresh=evalin('base','thresh');
    tic
    feats = featurize_im_for_RGB(img,feat_params);
    fprintf('featurisation = %.3fs\n',toc);
    % reduce resolution for speed
    img    = imresize(img   ,rez,'bilinear');
    feats  = imresize(feats ,rez,'bilinear');

    % reshape features
    [ly lx lz] = size(feats);
    feats = reshape(feats,ly*lx,lz);
    model_hash = repmat({[]},1000,1000);
    [ly lx lz] = size(img);
    if isempty(model_hash{ly,lx});
        model_hash{ly,lx} = gridmodel(ly,lx,nvals);
    end
    models = {};
    [ly lx lz] = size(img);
    models = model_hash{ly,lx};

    edge_params = {{'const'},{'diffthresh'},{'pairtypes'}};
    efeats = {};
    efeats = edgeify_im(img,edge_params,models.pairs,models.pairtype);
    loss_spec = 'trunc_cl_trwpll_5';
    crf_type  = 'linear_linear';
    [ly lx lz] = size(img);
    [b_i b_ij] = eval_crf(trained_model,feats,efeats,models,loss_spec,crf_type,rho);
    mask=reshape(b_i',ly,lx,nvals);

    prediction=zeros(ly,lx,1);
    [r,c]=find(mask(:,:,2)>thresh);
    for k=1:length(r)
        prediction(r(k),c(k))=1;
    end
    likelihood=mask(:,:,2);
    nDetection=length(r);
end

