function [ score ] = calc_accuracy_JAI_IR( A,B,str,saveFigure)
draw =0;
mycolormap=[[0 0 1];[1 1 0]];
tic
if(draw)
drawBackground=zeros([size(A) 3]);
drawCap=zeros([size(A) 3]);
end
gtBackground=zeros([size(A) 1]);
gtCap=zeros([size(A) 1]);

% Background is labled as '1';
% Capsicums are labled as '2';

estiBackgroundProb=B(:,:,1);
estiCapProb=B(:,:,2);


% Note that if you are using labels created from GIMP then
% A==1 for background, A==2 for Red, A==3 for Green
% Otherwise (MATLAB labeling tool), A==0 for background, A==1 for Red, A==2 for Green.
% This should be consistant and will be fixed later for using GIMP.

tic
[r,c]=find(A==1);
for i=1:length(r)
    if(draw)
    drawBackground(r(i),c(i),:)=mycolormap(1,:);
    end
    gtBackground(r(i),c(i))=1;
end
[r,c]=find(A==2);
for i=1:length(r)
    if(draw)
    drawCap(r(i),c(i),:)=mycolormap(2,:);
    end
    gtCap(r(i),c(i))=1;
end

toc
if(draw)
    figure;idisp(drawBackground+drawCap);
    figure;idisp(estiBackgroundProb);
    figure;idisp(estiCapProb);
end
%diffRedCap=gtRedCap-estiRedCapProb;


[X_cap,Y_cap,T_cap,AUC_cap] = perfcurve(gtCap(:),estiCapProb(:),1);
%[X_back,Y_back,T_back,AUC_back] = perfcurve(gtBackground(:),estiBackgroundProb(:),1);
figure;


p1=plot(X_cap,Y_cap);
set(p1,'Color','red','LineWidth',2);
strTitle=sprintf('Area Under Curve(AUC) calculation of %s',str);
t=title({strTitle});
% hold on;
% 
% p1=plot(X_back,Y_back,'k');
% set(p1,'Color','black','LineWidth',2);
% hold on;
ylabel('True positive rate');
xlabel('False positive rate');

legend_cap=sprintf('Capsicums, AUC=%.3f',AUC_cap);
%legend_bg=sprintf('Background, AUC=%.3f',AUC_back);
h_leg=legend(legend_cap);
set(legend,'position',[0.406117620549797,0.255914826498423,0.484046867958540,0.161923539131741]);
grid on;
if(saveFigure)
    n=evalin('base','n');
    saveFileName=sprintf('AUC_plot_n%d.pdf',n);
    saveTightFigure(gcf,saveFileName);
end
%%
score = AUC_cap;

end

