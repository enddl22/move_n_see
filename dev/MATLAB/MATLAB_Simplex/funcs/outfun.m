%Copyright (C) 2015, by Inkyu Sa, enddl22@gmail.com

%This is free software: you can redistribute it and/or modify
%it under the terms of the GNU Lesser General Public License as published by
%the Free Software Foundation, either version 3 of the License, or
%(at your option) any later version.
 
%This software package is distributed in the hope that it will be useful,
%but WITHOUT ANY WARRANTY; without even the implied warranty of
%MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%GNU Lesser General Public License for more details.

%You should have received a copy of the GNU Leser General Public License.
%If not, see <http://www.gnu.org/licenses/>.

function stop = outfun(x,~,state)
    global x_buf;
    %x_buf=evalin('base','x_buf');
    %fprintf('outputfun call\n');
    stop = false; 
    if strcmp(state,'iter')
        x_buf=[x_buf;x];
        disp([' x = ',num2str(x)]);
    end 


end

