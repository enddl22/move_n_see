clear all;
close all;
addpath('./data')
addpath('./funcs')
load('trj_n_Measure_timestamped.mat')
endeffectorTrj=myOut;
pos_x=endeffectorTrj(:,1);
pos_y=endeffectorTrj(:,2);
pos_z=endeffectorTrj(:,3);
m=endeffectorTrj(:,4);
time_stamp=endeffectorTrj(:,4);
x_min=min(pos_x);
x_max=max(pos_x);
y_min=min(pos_y);
y_max=max(pos_y);
z_min=min(pos_z);
z_max=max(pos_z);
display_on=1;
random_init=0; %otherwise use the first camera position
use_fminsearch=0; %otherwise use fminsearchbnd

global x_buf;
if random_init
    Init_th=[datasample(pos_x,1);
         datasample(pos_y,1);
         datasample(pos_z,1)];
else
    Init_th=[pos_x(1);pos_y(1);pos_z(1)];
end

options = optimset('Display','Iter','OutputFcn',@outfun);
if use_fminsearch
    [sol,fval,exitFlag,output]=fminsearch_modi(@evalfunc,[Init_th(1),Init_th(2),Init_th(3)],options);
    
    %The default fminsearch does not work.
    %[sol,fval,exitFlag,output]=fminsearch(@evalfunc,[Init_th(1),Init_th(2),Init_th(3)],options);
else
    x0=[Init_th(1),Init_th(2),Init_th(3)];
    LB=[x_min,y_min,z_min];
    UB=[x_max,y_max,z_max];
    [sol,fval,exitFlag,output]=fminsearchbnd(@evalfunc,x0,LB,UB,options);
end
exitFlag

if display_on
    fig=figure;
    myaxis=gca;
    myColor=jet(length(m));
    scatter3(myaxis,pos_x,pos_y,pos_z,200,myColor);
    colorbar;
    colormap jet;
    hold on;
%     T = [150,   150,   150
%          255, 0, 0]./255;
%     x = [0
%          255];
%     MyColorMap = interp1(x/length(x_buf),T,linspace(0,1,255));
%     MyColorMap=copper(length(x_buf));
    plot3(myaxis,x_buf(:,1),x_buf(:,2),x_buf(:,3),'MarkerSize',15,...
             'Marker','o','LineWidth',3,'LineStyle','-','Color','r');
%     hold on;
%     for i=1:length(x_buf)
%          plot3(myaxis,x_buf(i,1),x_buf(i,2),x_buf(i,3),'MarkerSize',15,...
%               'MarkerFaceColor',MyColorMap(i,:),'Marker','o','MarkerEdgeColor',...
%               MyColorMap(i,:));
%          hold on;
%     end
end
[gt_px,gt_idx]=max(m);

fprintf('Init x=%.2f, Init y=%.2f, Init z=%.2f, Init m=%.2f\n',Init_th(1),Init_th(2),Init_th(3),m(1));
fprintf('GT x=%.2f, GT y=%.2f, GT z=%.2f, GT m=%.2f\n',pos_x(gt_idx),pos_y(gt_idx),pos_z(gt_idx),gt_px);
fprintf('sol x=%.2f, sol y=%.2f, sol z=%.2f, sol val=%.2f\n',sol(1),sol(2),sol(3),fval*-1);
fprintf('err x=%.2f, err y=%.2f, err z=%.2f,\n',pos_x(gt_idx)-sol(1),pos_y(gt_idx)-sol(2),pos_z(gt_idx)-sol(3));

