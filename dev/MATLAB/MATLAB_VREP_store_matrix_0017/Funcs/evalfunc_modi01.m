% Copyright (C) 2015, by Inkyu Sa, enddl22@gmail.com
%
% This is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This software package is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Leser General Public License.
% If not, see <http://www.gnu.org/licenses/>.

function f = evalfunc_modi01(input)
    global g_prev_pose;
    global g_prev_pose_buf;
    global g_init_pose;
    
    vrep=evalin('base','vrep');
    thresh_dist=evalin('base','thresh_dist');
    endeffectorhandle=evalin('base','endeffectorhandle');
    clientID=evalin('base','clientID');
    camhandle=evalin('base','camhandle');
    endeffectorTrj = evalin('base','endeffectorTrj');
    draw_trj = evalin('base','draw_trj');
    draw_img = evalin('base','draw_img');
    draw_detection = evalin('base','draw_detection');
    draw_decMap = evalin('base','draw_decMap');
    img_cnt = evalin('base','img_cnt');
    save_img_to_file = evalin('base','save_img_to_file');
    detec_buf=evalin('base','detec_buf');
    detection_cnt=evalin('base','detection_cnt');
    time_buf=evalin('base','time_buf');
    start_time=evalin('base','start_time');
    
    
    if draw_trj
        hFig_Trj=evalin('base','hFig_Trj');
        hTrj=evalin('base','hTrj');
    end
    if draw_img
        hFig_Img=evalin('base','hFig_Img');
        hImg=evalin('base','hImg');
    end
    if draw_detection
        hFig_Detec=evalin('base','hFig_Detec');
        hDetectionImg=evalin('base','hDetectionImg');
    end
    if draw_decMap
        hFig_DecMap=evalin('base','hFig_DecMap');
        hDetectionMap=evalin('base','hDetectionMap');
    end
  
    %============================
    %   Move and see needs here!
    %============================
    cur_dist=sqrt( (g_prev_pose(1)-input(1))^2+ ...
            (g_prev_pose(2)-input(2))^2+...
            (g_prev_pose(3)-input(3))^2     );
    if (cur_dist>=thresh_dist)
        sendPositionCmd(clientID,vrep,input'-g_init_pose);
        [errorCode,position]=vrep.simxGetObjectPosition(clientID,endeffectorhandle,-1,vrep.simx_opmode_buffer);
        endeffectorTrj = [endeffectorTrj;position];
        assignin('base','endeffectorTrj',endeffectorTrj);
        if draw_trj
            set(0,'CurrentFigure',hFig_Trj);
            for r=1:size(endeffectorTrj,1)
            drawCamWithFrustum(hTrj,endeffectorTrj(r,1),endeffectorTrj(r,2),endeffectorTrj(r,3),0,0,0);
            view(-44,22);
            end
        end 

        [errorCode,resolution,img]=vrep.simxGetVisionSensorImage2(clientID,camhandle,0,vrep.simx_opmode_oneshot_wait);
        if save_img_to_file
            img_name=sprintf('./out/pngs/RGB_img%04d.png',img_cnt);
            imwrite(img,img_name);
            img_cnt=img_cnt+1;
            assignin('base','img_cnt',img_cnt);
        end
        if draw_img
            set(0,'CurrentFigure',hFig_Img);
            imshow(img,'parent',hImg);
        end

        [likelihood,prediction,nDetection]=detection(double(img));
        nDetection
        
        if draw_detection
            set(0,'CurrentFigure',hFig_Detec);
            imshow(prediction,'parent',hDetectionImg);
        end
        time_buf(detection_cnt)=toc(start_time);
        detection_cnt=detection_cnt+1;
        detec_buf=[detec_buf;nDetection];
        
        
        assignin('base','time_buf',time_buf);
        assignin('base','detection_cnt',detection_cnt);
        assignin('base','detec_buf',detec_buf);
        
        if draw_decMap
            set(0,'CurrentFigure',hFig_DecMap);
            drawDetectionMap(hDetectionMap,endeffectorTrj(:,1),endeffectorTrj(:,2),endeffectorTrj(:,3),detec_buf);
        end
        g_prev_pose=input';
        g_prev_pose_buf=[g_prev_pose_buf g_prev_pose];
        f=nDetection*-1;
    else
        f=nan;
    end
end

