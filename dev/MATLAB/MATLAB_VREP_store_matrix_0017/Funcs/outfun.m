%Copyright (C) 2015, by Inkyu Sa, enddl22@gmail.com

%This is free software: you can redistribute it and/or modify
%it under the terms of the GNU Lesser General Public License as published by
%the Free Software Foundation, either version 3 of the License, or
%(at your option) any later version.
 
%This software package is distributed in the hope that it will be useful,
%but WITHOUT ANY WARRANTY; without even the implied warranty of
%MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%GNU Lesser General Public License for more details.

%You should have received a copy of the GNU Leser General Public License.
%If not, see <http://www.gnu.org/licenses/>.

function stop = outfun(x,optimValues,state)
    global x_buf;
    global g_prev_sol;
    global g_stop_cnt;
    global g_prev_f;
    if g_stop_cnt>=20
        stop=true;
    else
        stop = false; 
    end
    if strcmp(state,'iter')
        x_buf=[x_buf;x];
        disp([' x = ',num2str(x)]);
        g_prev_sol=x;
        g_prev_f
        optimValues.fval
        if g_prev_f==optimValues.fval
            g_stop_cnt=g_stop_cnt+1;
        else
            g_prev_f=optimValues.fval;
            g_stop_cnt=0;
        end      
    end  
    g_stop_cnt
end

