function [ output_args ] = drawCamWithFrustum(h, x,y,z,roll,pitch,yaw )
%drawCamWithFrustum A drawing function of 3D camera in SE(3) space with
%frustum.
%
% input: x,y,z,roll,pitch,yaw 
% The SI units are applied (meter,rad) and all coordinates are governing under 
% the right-hand rule.
% x axis: red, front
% y axis: green, left
% z axis: blue, up
%
% output: none
%
%
% Options: none
% Note: The code is now quite aguly and will be cleaned up asap.

% Copyright (C) 2015, by Inkyu Sa, enddl22@gmail.com
%
% This is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This software package is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Leser General Public License.
% If not, see <http://www.gnu.org/licenses/>.

    length = 0.03;


    % From peter's toolbox
    R = rpy2r(roll,pitch,yaw);

    % generate axis vectors
    tx = [length,0.0,0.0];
    ty = [0.0,length,0.0];
    tz = [0.0,0.0,length];
    % Rotate it by R
    t_x_new = R*tx';
    t_y_new = R*ty';
    t_z_new = R*tz';
    
    
    
    % translate vectors to camera position. Make the vectors for plotting
    origin=[x,y,z];
    tx_vec(1,1:3) = origin;
    tx_vec(2,:) = t_x_new + origin';
    ty_vec(1,1:3) = origin;
    ty_vec(2,:) = t_y_new + origin';
    tz_vec(1,1:3) = origin;
    tz_vec(2,:) = t_z_new + origin';
    hold on;
    
    % Plot the direction vectors at the point
    p1=plot3(h,tx_vec(:,1), tx_vec(:,2), tx_vec(:,3));
    set(p1,'Color','Red','LineWidth',1);
    p1=plot3(h,ty_vec(:,1), ty_vec(:,2), ty_vec(:,3));
    set(p1,'Color','Green','LineWidth',1);
    p1=plot3(h,tz_vec(:,1), tz_vec(:,2), tz_vec(:,3));
    set(p1,'Color','Blue','LineWidth',1);
    axis equal;
    grid on;
    % Drawing frustum
    
    %frustum_length=0.15;
    %frustum_height=0.2;
    
    frustum_length=length; % This defines the near-depth and far-depth
    frustum_height=length;
    
    frustum_angle=deg2rad(45); % Filed-of-view of a camera.
    
    % generate axis vectors
    tx_frustum = [frustum_length,0.0,0.0];
    ty_frustum = [0.0,frustum_length,0.0];
    tz_frustum = [0.0,0.0,frustum_length];
    % From peter's toolbox
    R1 = rpy2r(-frustum_angle,0,-frustum_angle);
    R2 = rpy2r(frustum_angle,0,frustum_angle);
    R3 = rpy2r(frustum_angle,0,-frustum_angle);
    R4 = rpy2r(-frustum_angle,0,frustum_angle);
    
    % Rotate it by R
    t_x_new_frustum = R*R1*tx_frustum';
    t_y_new_frustum = R*R1*ty_frustum';
    t_z_new_frustum = R*R1*tz_frustum';
    
    % translate vectors to camera position. Make the vectors for plotting
    origin=[x,y,z];
    tx_vec_frustum(1,1:3) = origin;
    tx_vec_frustum(2,:) = t_x_new_frustum + origin';
    ty_vec_frustum(1,1:3) = origin;
    ty_vec_frustum(2,:) = t_y_new_frustum + origin';
    tz_vec_frustum(1,1:3) = origin;
    tz_vec_frustum(2,:) = t_z_new_frustum + origin';
    %hold on;

    
    frustum_vertices=origin';
    % Plot the direction vectors at the point
    p1=plot3(h,tx_vec_frustum(:,1), tx_vec_frustum(:,2), tx_vec_frustum(:,3));
    set(p1,'Color','Magenta','LineWidth',1);
    frustum_vertices=[ frustum_vertices tx_vec_frustum(2,:)'];
    % Rotate it by R
    t_x_new_frustum = R*R2*tx_frustum';
    t_y_new_frustum = R*R2*ty_frustum';
    t_z_new_frustum = R*R2*tz_frustum';
    
    % translate vectors to camera position. Make the vectors for plotting
    origin=[x,y,z];
    tx_vec_frustum(1,1:3) = origin;
    tx_vec_frustum(2,:) = t_x_new_frustum + origin';
    ty_vec_frustum(1,1:3) = origin;
    ty_vec_frustum(2,:) = t_y_new_frustum + origin';
    tz_vec_frustum(1,1:3) = origin;
    tz_vec_frustum(2,:) = t_z_new_frustum + origin';
    %hold on;
    
    % Plot the direction vectors at the point
    p1=plot3(h,tx_vec_frustum(:,1), tx_vec_frustum(:,2), tx_vec_frustum(:,3));
    set(p1,'Color','Black','LineWidth',1);
    frustum_vertices=[ frustum_vertices tx_vec_frustum(2,:)'];
    % Rotate it by R
    t_x_new_frustum = R*R3*tx_frustum';
    t_y_new_frustum = R*R3*ty_frustum';
    t_z_new_frustum = R*R3*tz_frustum';
    
    % translate vectors to camera position. Make the vectors for plotting
    origin=[x,y,z];
    tx_vec_frustum(1,1:3) = origin;
    tx_vec_frustum(2,:) = t_x_new_frustum + origin';
    ty_vec_frustum(1,1:3) = origin;
    ty_vec_frustum(2,:) = t_y_new_frustum + origin';
    tz_vec_frustum(1,1:3) = origin;
    tz_vec_frustum(2,:) = t_z_new_frustum + origin';
    %hold on;
    
    % Plot the direction vectors at the point
    p1=plot3(h,tx_vec_frustum(:,1), tx_vec_frustum(:,2), tx_vec_frustum(:,3));
    set(p1,'Color','Black','LineWidth',1);
    frustum_vertices=[ frustum_vertices tx_vec_frustum(2,:)'];
    
    % Rotate it by R
    t_x_new_frustum = R4*tx_frustum';
    t_y_new_frustum = R4*ty_frustum';
    t_z_new_frustum = R4*tz_frustum';
    
    % translate vectors to camera position. Make the vectors for plotting
    origin=[x,y,z];
    tx_vec_frustum(1,1:3) = origin;
    tx_vec_frustum(2,:) = t_x_new_frustum + origin';
    ty_vec_frustum(1,1:3) = origin;
    ty_vec_frustum(2,:) = t_y_new_frustum + origin';
    tz_vec_frustum(1,1:3) = origin;
    tz_vec_frustum(2,:) = t_z_new_frustum + origin';
    %hold on;
    
    % Plot the direction vectors at the point
    % Side faces
    p1=plot3(h,tx_vec_frustum(:,1), tx_vec_frustum(:,2), tx_vec_frustum(:,3));
    set(p1,'Color','Black','LineWidth',1);
    frustum_vertices=[ frustum_vertices tx_vec_frustum(2,:)'];
    p1 = struct('vertices',frustum_vertices','faces',[1 4 2;1 4 5;1 2 3;1 5 3]);
    h1 = patch(p1);
    set(h1,'facecolor',[0 142 41]/255,'FaceAlpha',0.1,'EdgeColor', 'k');
    
    % Plot the direction vectors at the point
    % The front face
    p2 = struct('vertices',frustum_vertices','faces',[2 3 5;2 4 5]);
    h2 = patch(p2);
    %set(h2,'facecolor',[236 171 76]/255,'EdgeColor', 'none');
    set(h2,'facecolor',[0 220 41]/255,'FaceAlpha',0.7,'EdgeColor', 'none');
    hold off;
    
end

