% Copyright (C) 2015, by Inkyu Sa, enddl22@gmail.com
%
% This is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This software package is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Leser General Public License.
% If not, see <http://www.gnu.org/licenses/>.
clear all;
close all;
addpath(genpath('../MATLAB_VREP_store_matrix_0017'));
vrep=remApi('remoteApi'); % using the prototype file (remoteApiProto.m)
vrep.simxFinish(-1); % just in case, close all opened connections
clientID=vrep.simxStart('127.0.0.1',19997,true,true,5000,5);
camhandle=[];
endeffectorhandle=[];
endeffectorTrj=[];
leftArmTarget_handle=[];
imgs={};
init_pos=[0,0,0];
mkdir('./out/mats/');
mkdir('./out/pngs/');

use_ros=0;
if use_ros
    rosinit
end
if use_ros
    detectionMsg=rosmessage('std_msgs/UInt64');
    detectionImgMsg=rosmessage('sensor_msgs/Image');
    detectionImgMsg.Encoding='mono8';
    MeasurePub = rospublisher('/MATLAB/detector','std_msgs/UInt64');
    ImgPub = rospublisher('/MATLAB/image','sensor_msgs/Image');
end
nvals  = 2;
rez    = 0.3; % how much to reduce resolution
rho    = 0.5; % (1 = loopy belief propagation) (.5 = tree-reweighted belief propagation)
feat_params = {{'hsvpatches',0},{'fourier',1},{'hog',32},{'lbp'}};
load('p_hsv_fourier_hog_lbp.mat');
load('thresh.mat');
trained_model=p;
detection_out={};
detec_buf=[];
time_buf=[];
    
start_time=0;
detection_cnt=1;
img_cnt=1;

%==========================
%       Plot control flags
%==========================

draw_detection=0;
draw_img=0;
draw_trj=0;
draw_decMap=0;
save_img_to_file=0;


global g_prev_pose;
global g_init_pose;
global g_prev_sol;
global g_stop_cnt;
global g_prev_f;
    

Init_th=[init_pos(1);init_pos(2);init_pos(3)];
g_prev_pose=Init_th;
g_init_pose=Init_th;
g_prev_sol=Init_th;
g_prev_f=-135;
g_img_cnt=1;
x0=[Init_th(1),Init_th(2),Init_th(3)];
LB=[-1.20736467838287,0.475041717290878,1.26525676250458];
UB=[-1.0073162317276,0.675015687942505,1.345374584198];
options = optimset('Display','Iter','OutputFcn',@outfun);
thresh_dist=1e-3; %20cm
g_stop_cnt=0;          

if draw_trj
    hFig_Trj=figure('Name','Camera Trajectory');
    hTrj=axes;
end
if draw_img
    hFig_Img=figure('Name','RGB Image');
    hImg=axes;
end

if draw_detection
    hFig_Detec=figure('Name','Pixel Segmentation(prediction)');
    hDetectionImg=axes;
end

if draw_decMap
    hFig_DecMap=figure('Name','Detection Map');
    hDetectionMap=axes;
end


try
	if (clientID>-1)
		disp('Connected to remote API server');
        disp('Starting simulation');
        
        [res,objs]=vrep.simxGetObjects(clientID,vrep.sim_handle_all,vrep.simx_opmode_oneshot_wait);
        if (res==vrep.simx_error_noerror)
            [err,camhandle]=vrep.simxGetObjectHandle(clientID,'Baxter_leftArm_camera',vrep.simx_opmode_oneshot_wait);
            [err,endeffectorhandle]= vrep.simxGetObjectHandle(clientID,'Baxter_leftArm_tip',vrep.simx_opmode_oneshot_wait);
            [err,leftArmTarget_handle]= vrep.simxGetObjectHandle(clientID,'Baxter_leftArm_mpTarget1',vrep.simx_opmode_oneshot_wait);
            
        else
            fprintf('Remote API function call returned with error code: %d\n',res);
        end
    
        vrep.simxStartSimulation(clientID,vrep.simx_opmode_oneshot_wait);
        pause(2);
        
        while(1)
            if sum(abs(init_pos))>0
                break;
            else
               [errorCode,init_pos]=vrep.simxGetObjectPosition(clientID,leftArmTarget_handle,-1,vrep.simx_opmode_streaming);
            end
        end
        %pause();  
        sendPositionCmd(clientID,vrep,init_pos);
        [errorCode,position]=vrep.simxGetObjectPosition(clientID,endeffectorhandle,-1,vrep.simx_opmode_streaming);
        g_prev_pose=init_pos';
        g_init_pose=init_pos';
        g_prev_sol=init_pos';
        g_prev_f=-135; 
        x0=double([init_pos(1),init_pos(2),init_pos(3)]);
        pause();  
        [errorCode,resolution,img]=vrep.simxGetVisionSensorImage2(clientID,camhandle,0,vrep.simx_opmode_oneshot_wait);
        [likelihood,prediction,nDetection]=detection(double(img));
        nDetection
        g_prev_f=-nDetection;
        start_time=tic;
        
        
        
        [sol,fval,exitFlag,output]=fminsearch_modi(@evalfunc_modi01,x0,options);
        sendPositionCmd(clientID,vrep,sol'-g_init_pose);
        pause();        
        returnCode=vrep.simxStopSimulation(clientID,vrep.simx_opmode_oneshot_wait);
        pause(0.5);
        vrep.simxFinish(clientID); % close the line if still open
        pause(0.5);
	else
		disp('Failed connecting to remote API server');
	end
	vrep.delete(); % call the destructor!
    if use_ros
        rosshutdown;
    end
    %pause();
catch err
    returnCode=vrep.simxStopSimulation(clientID,vrep.simx_opmode_oneshot_wait);
	vrep.simxFinish(clientID); % close the line if still open
	vrep.delete(); % call the destructor!
    if use_ros
        rosshutdown;
    end
end;

disp('Program ended');
