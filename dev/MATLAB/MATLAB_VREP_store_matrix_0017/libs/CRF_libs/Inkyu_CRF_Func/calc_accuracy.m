function [ score ] = calc_accuracy( A,B,str,saveFigure)
draw =0;
mycolormap=[[0 0 1];[1 1 0];[1 0 0]];
tic
if(draw)
drawBackground=zeros([size(A) 3]);
drawRedCap=zeros([size(A) 3]);
drawGreenCap=zeros([size(A) 3]);
end
gtBackground=zeros([size(A) 1]);
gtRedCap=zeros([size(A) 1]);
gtGreenCap=zeros([size(A) 1]);

% Background is labled as '1';
% Redcapsicums are labled as '2';
% Greencapsicums are labled as '3';

estiBackgroundProb=B(:,:,1);
estiRedCapProb=B(:,:,2);
estiGreenCapProb=B(:,:,3);



% Note that if you are using labels created from GIMP then
% A==1 for background, A==2 for Red, A==3 for Green
% Otherwise (MATLAB labeling tool), A==0 for background, A==1 for Red, A==2 for Green.
% This should be consistant and will be fixed later for using GIMP.

tic
[r,c]=find(A==1);
for i=1:length(r)
    if(draw)
    drawBackground(r(i),c(i),:)=mycolormap(1,:);
    end
    gtBackground(r(i),c(i))=1;
end
[r,c]=find(A==2);
for i=1:length(r)
    if(draw)
    drawRedCap(r(i),c(i),:)=mycolormap(2,:);
    end
    gtRedCap(r(i),c(i))=1;
end
[r,c]=find(A==3);
for i=1:length(r)
    if(draw)
    drawGreenCap(r(i),c(i),:)=mycolormap(3,:);
    end
    gtGreenCap(r(i),c(i))=1;
end
toc
if(draw)
    figure;idisp(drawBackground+drawRedCap+drawGreenCap);
    figure;idisp(estiBackgroundProb);
    figure;idisp(estiRedCapProb);
    figure;idisp(estiGreenCapProb);
end
%diffRedCap=gtRedCap-estiRedCapProb;

%%
[X_red,Y_red,T_red,AUC_red] = perfcurve(gtRedCap(:),estiRedCapProb(:),1);
[X_green,Y_green,T_green,AUC_green] = perfcurve(gtGreenCap(:),estiGreenCapProb(:),1);
[X_back,Y_back,T_back,AUC_back] = perfcurve(gtBackground(:),estiBackgroundProb(:),1);
figure;


p1=plot(X_red,Y_red);
set(p1,'Color','red','LineWidth',2);
strTitle=sprintf('Area Under Curve(AUC) calculation of %s',str);
t=title({strTitle});
hold on;

p1=plot(X_green,Y_green,'--');
set(p1,'Color','b','LineWidth',2);
hold on;

p1=plot(X_back,Y_back,'k');
set(p1,'Color','black','LineWidth',2);
hold on;
ylabel('True positive rate');
xlabel('False positive rate');
legend_red=sprintf('Red capsicums, AUC=%.3f',AUC_red);
legend_green=sprintf('Green capsicums, AUC=%.3f',AUC_green);
legend_bg=sprintf('Background, AUC=%.3f',AUC_back);
h_leg=legend(legend_red,legend_green,legend_bg);
set(legend,'position',[0.406117620549797,0.255914826498423,0.484046867958540,0.161923539131741]);
grid on;
if(saveFigure)
    n=evalin('base','n');
    saveFileName=sprintf('AUC_plot_n%d.pdf',n);
    saveTightFigure(gcf,saveFileName);
end
%%
score = [AUC_back AUC_red AUC_green];

end

