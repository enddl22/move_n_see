% Copyright (C) 2015, by Inkyu Sa, enddl22@gmail.com
%
% This is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This software package is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Leser General Public License.
% If not, see <http://www.gnu.org/licenses/>.
clear all;
close all;
addpath(genpath('../MATLAB_VREP_IBVS_0016'));
vrep=remApi('remoteApi'); % using the prototype file (remoteApiProto.m)
vrep.simxFinish(-1); % just in case, close all opened connections
clientID=vrep.simxStart('127.0.0.1',19997,true,true,5000,5);
camhandle=[];
endeffectorhandle=[];
endeffectorTrj=[];
leftArmTarget_handle=[];
imgs={};
init_pos=[0,0,0];
%init_pos=[-1.1074706,0.57490987+0.45,1.2454941];
img_cnt=1;
mkdir('./out/mats/');
mkdir('./out/pngs/');


                
cam= CentralCamera('focal', 0.003, 'pixel', 10e-6,'resolution', [640 480],... 
                    'centre', [320 240], 'name', 'mycamera');
capsicum_target_width=0.025;
lambda=0.001;
depth=1;
threshold=55;
max_iteration=10000;
capsicum_target_pos=[0,0,0];
init_cam_pose=[0,0,0];
cur_cam_pose=[0,0,0];
use_ros=0;
if use_ros
    rosinit
end
%endPose=zeros(7,1);
if use_ros
    detectionMsg=rosmessage('std_msgs/UInt64');
    detectionImgMsg=rosmessage('sensor_msgs/Image');
    detectionImgMsg.Encoding='mono8';
    %MeasurePub = rospublisher('/MATLAB/detector',rostype.std_msgs_UInt64);
    MeasurePub = rospublisher('/MATLAB/detector','std_msgs/UInt64');
    ImgPub = rospublisher('/MATLAB/image','sensor_msgs/Image');
end
nvals  = 2;
rez    = 0.3; % how much to reduce resolution
rho    = 0.5; % (1 = loopy belief propagation) (.5 = tree-reweighted belief propagation)
feat_params = {{'hsvpatches',0},{'fourier',1},{'hog',32},{'lbp'}};
%feat_params = {{'hsvpatches',0}};
load('p_hsv_fourier_hog_lbp.mat');
load('thresh.mat');
trained_model=p;
detection_out={};
detec_buf=[];
time_buf=[];
start_time=0;
detection_cnt=1;


draw_detection=0;
draw_img=0;
draw_trj=0;
draw_decMap=0;

global g_prev_f;
global g_pos;
global g_prev_pose;
global g_init_pose;
global g_prev_sol;
global g_stop_cnt;
cmd_vel_buf=[];
sum_feature_err_buf=[];
cam_pose_buf=[];

wRc=roty(pi/2)*rotz(pi/2);
Init_th=[init_pos(1);init_pos(2);init_pos(3)];
g_prev_pose=Init_th;
g_init_pose=Init_th;
g_prev_sol=Init_th;
g_prev_f=-135; 
x0=[Init_th(1),Init_th(2),Init_th(3)];
LB=[-1.20736467838287,0.475041717290878,1.26525676250458];
UB=[-1.0073162317276,0.675015687942505,1.345374584198];
options = optimset('Display','Iter','OutputFcn',@outfun);
g_pos=[0;0;0];
thresh_dist=1e-3; %20cm
g_stop_cnt=0;          

          
        
        

if draw_trj
    hFig_Trj=figure('Name','Camera Trajectory');
    hTrj=axes;
end
if draw_img
    hFig_Img=figure('Name','RGB Image');
    hImg=axes;
end

if draw_detection
    hFig_Detec=figure('Name','Pixel Segmentation(prediction)');
    hDetectionImg=axes;
end

if draw_decMap
    hFig_DecMap=figure('Name','Detection Map');
    hDetectionMap=axes;
end


try
	if (clientID>-1)
		disp('Connected to remote API server');
        disp('Starting simulation');
        
        [res,objs]=vrep.simxGetObjects(clientID,vrep.sim_handle_all,vrep.simx_opmode_oneshot_wait);
        if (res==vrep.simx_error_noerror)
            [err,camhandle]=vrep.simxGetObjectHandle(clientID,'Baxter_leftArm_camera',vrep.simx_opmode_oneshot_wait);
            [err,endeffectorhandle]= vrep.simxGetObjectHandle(clientID,'Baxter_leftArm_tip',vrep.simx_opmode_oneshot_wait);
            [err,leftArmTarget_handle]= vrep.simxGetObjectHandle(clientID,'Baxter_leftArm_mpTarget1',vrep.simx_opmode_oneshot_wait);
            [err,capsicumTarget_handle]= vrep.simxGetObjectHandle(clientID,'CapsicumTarget',vrep.simx_opmode_oneshot_wait);
        else
            fprintf('Remote API function call returned with error code: %d\n',res);
        end
    
        vrep.simxStartSimulation(clientID,vrep.simx_opmode_oneshot_wait);
        pause(2);
        
        while(1)
            if sum(abs(init_pos))>0
                break;
            else
               [errorCode,init_pos]=vrep.simxGetObjectPosition(clientID,leftArmTarget_handle,-1,vrep.simx_opmode_streaming);
                init_pos
            end
        end
        while(1)
            if sum(abs(capsicum_target_pos))>0
                break;
            else
               [errorCode,capsicum_target_pos]=vrep.simxGetObjectPosition(clientID,capsicumTarget_handle,-1,vrep.simx_opmode_streaming);
                capsicum_target_pos
            end
        end
       
        P=[[capsicum_target_pos(1);capsicum_target_pos(2)+capsicum_target_width;capsicum_target_pos(3)+capsicum_target_width],...
           [capsicum_target_pos(1);capsicum_target_pos(2)-capsicum_target_width;capsicum_target_pos(3)+capsicum_target_width],...
           [capsicum_target_pos(1);capsicum_target_pos(2)+capsicum_target_width;capsicum_target_pos(3)-capsicum_target_width],...
           [capsicum_target_pos(1);capsicum_target_pos(2)-capsicum_target_width;capsicum_target_pos(3)-capsicum_target_width]
           ];
        
        %P2=mkgrid(2,0.5,'T',transl(capsicum_target_pos(1),capsicum_target_pos(2),capsicum_target_pos(3)));
        
        
        %pause();  
        

        sendPositionCmd(clientID,vrep,init_pos);
        pause();
        while(1)
            if sum(abs(init_cam_pose))>0
                break;
            else
                [errorCode,init_cam_pose]=vrep.simxGetObjectPosition(clientID,endeffectorhandle,-1,vrep.simx_opmode_streaming);
                
                init_cam_pose
            end
        end 
        
        %init_cam_pose=position;
         %0.1m away from the target
        goal_cam_pose=transl(-0.1,0,0)...
                *transl(capsicum_target_pos(1), capsicum_target_pos(2), ...
                capsicum_target_pos(3))*r2t(roty(pi/2)*rotz(pi));
        pStar=cam.project(P,'Tcam',goal_cam_pose);
        cam.T=transl(init_cam_pose(1),init_cam_pose(2),init_cam_pose(3))*r2t(roty(pi/2)*rotz(pi));
        cur_p=cam.project(P,'Tcam',cam.T);
        feature_err=pStar-cur_p;
        for i=1:max_iteration
            if(sum(feature_err(:))^2>threshold)
                sum_feature_err_buf=[sum_feature_err_buf sum(feature_err(:))^2];
                [errorCode,cur_cam_pose]=vrep.simxGetObjectPosition(clientID,endeffectorhandle,-1,vrep.simx_opmode_streaming);
                cam.T=transl(cur_cam_pose(1),cur_cam_pose(2),cur_cam_pose(3))*r2t(roty(pi/2)*rotz(pi));
                cur_p=cam.project(P,'Tcam',cam.T); 
                feature_err=pStar-cur_p;
                J=visjac_p(cam,cur_p,depth);
                v=lambda*pinv(J)*feature_err(:);
                cmd_vel_buf=[cmd_vel_buf v];
                cam_pose_buf=[cam_pose_buf transl(cam.T)];
                cmd_v=roty(pi/2)*rotz(pi)*v(1:3);
                sendVelocityCmd(clientID,vrep,cmd_v);
            else
                pause();
                figure;plot(cam_pose_buf')
                figure;plot(cmd_vel_buf')
                figure;plot(sum_feature_err_buf')

            end
            
        end
        
        
%           cam.plot(P);
%           cam.hold;
%           cam.plot(pStar,'r*');
          
            



        
        returnCode=vrep.simxStopSimulation(clientID,vrep.simx_opmode_oneshot_wait);
        pause(0.5);
        vrep.simxFinish(clientID); % close the line if still open
        pause(0.5);
	else
		disp('Failed connecting to remote API server');
	end
	vrep.delete(); % call the destructor!
    if use_ros
        rosshutdown;
    end
    %pause();
catch err
    returnCode=vrep.simxStopSimulation(clientID,vrep.simx_opmode_oneshot_wait);
	vrep.simxFinish(clientID); % close the line if still open
	vrep.delete(); % call the destructor!
    if use_ros
        rosshutdown;
    end
end;

disp('Program ended');
