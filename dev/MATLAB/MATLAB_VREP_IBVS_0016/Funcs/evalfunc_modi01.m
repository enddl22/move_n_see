function f = evalfunc_modi01(input)
    global g_cnt_eval;
    global g_prev_pose;
    global g_prev_pose_buf;
    %global g_prev_f;
    global g_init_pose;
%     pos_x=evalin('base','pos_x');
%     pos_y=evalin('base','pos_y');
%     pos_z=evalin('base','pos_z');
    vrep=evalin('base','vrep');
%     m=evalin('base','m');
    thresh_dist=evalin('base','thresh_dist');
    endeffectorhandle=evalin('base','endeffectorhandle');
    clientID=evalin('base','clientID');
    camhandle=evalin('base','camhandle');
    %position=[];
%     dist=[];
%     for i=1:length(pos_x)
%         dist(i)=sqrt( (pos_x(i)-input(1))^2+ ...
%             (pos_y(i)-input(2))^2+...
%             (pos_z(i)-input(3))^2     );
%     end
%     [~,min_idx]=min(dist);
    %min_idx
    
    %============================
    %   Move and see needs here!
    %============================
    
    
    g_cnt_eval=g_cnt_eval+1;
    cur_dist=sqrt( (g_prev_pose(1)-input(1))^2+ ...
            (g_prev_pose(2)-input(2))^2+...
            (g_prev_pose(3)-input(3))^2     );
    if (cur_dist>=thresh_dist)
        sendPositionCmd(clientID,vrep,input'-g_init_pose);
        [errorCode,position]=vrep.simxGetObjectPosition(clientID,endeffectorhandle,-1,vrep.simx_opmode_buffer);
        
        [errorCode,resolution,img]=vrep.simxGetVisionSensorImage2(clientID,camhandle,0,vrep.simx_opmode_oneshot_wait);
        [likelihood,prediction,nDetection]=detection(double(img));
        nDetection
        g_prev_pose=input';
        g_prev_pose_buf=[g_prev_pose_buf g_prev_pose];
        f=nDetection*-1;
        %g_prev_f=f;
    else
        f=nan;
    end
    %printf('pos_x=%0.3f,pos_y=%0.3f,pos_z=%0.3f\n',pos_x,pos_y,pos_z);  
    
    %vq=evalin('base','vq');
%     tmp_x=abs(input(1)-x);
%     [~,idx_x]=min(tmp_x);
%     tmp_y=abs(input(2)-y);
%     [~,idx_y]=min(tmp_y);
%     tmp_z=abs(input(1)-z);
%     [~,idx_z]=min(tmp_z);
    
       
    %x(1)=idx_x;
    %x(2)=idx_y;
    %x(2)=find(x(2)==search_range_y_intp);
%     f=vq(idx_x,idx_y); 
end

