% Copyright (C) 2015, by Inkyu Sa, enddl22@gmail.com
%
% This is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This software package is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Leser General Public License.
% If not, see <http://www.gnu.org/licenses/>.

function [ errorCode ] = sendVelocityCmd( clientID, vrep,vel )
%SENDPOSITIONCMD Summary of this function goes here
%   Detailed explanation goes here
    %Note that position should be 1x3 matrix not 3x1.
    s=size(vel);
    if(s(1)==3)
        vel=vel';
    end
    velocityString=vrep.simxPackFloats(vel);
    %pause(1);
    errorCode=vrep.simxSetStringSignal(clientID,'VelocityEndeffector',velocityString,vrep.simx_opmode_oneshot_wait);
    % http://www.forum.coppeliarobotics.com/viewtopic.php?f=9&t=1239
    % Very importatn delay!!!!
    %vrep.simxGetPingTime();
    %pause(0.5);
end

