#! /usr/bin/python
"""
Created on Fri May 29 14:58:12 2015

@author: feras
"""

import sys
import os
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import time


fig, (ax,ax1) = plt.subplots(1,2,subplot_kw=dict(projection='3d'))

plt.ion()
plt.grid()
fig.show()

poses = np.load('./pick.npy')


def get_maxlk(idx_p,max_th):
    data = np.array([])
    t = 0    
    
    scores = poses[:,3]
    scores = (scores - np.min(scores)) / (np.max(scores) - np.min(scores))
    poses[:,3] = scores * 100
    
    for p in poses[idx_p,:]:      
        mu=np.array([p[0] , p[1] , p[2]])
        s = 0.001 * p[3] * 0.01
        #sigma=np.matrix([[0,s,s],[s,0,s],[s,s,s]])
        sigma=np.matrix([[s,0,0],
                         [0,s,0],
                         [0,0,s]])
        dist = np.random.multivariate_normal(mu,sigma,int(p[3]))
        t += dist.shape[0]
        data = np.append(data,dist)
    
    data = np.reshape(data,(-1,3))
    values = data.T
    kde = stats.gaussian_kde(values)
    density = kde(values)
    x, y, z = values
    idx_m = [density>=(max(density)*max_th)]
    idx = [density>=(max(density)) * 0.95]   
    
    x_m = np.mean(x[idx])
    y_m = np.mean(y[idx])
    z_m = np.mean(z[idx])

    return [x_m,y_m,z_m], idx_m , values , density

def find_closest(ps , p):
    d = 100
    ri = 0
    for i , rp in enumerate(ps):               
        dt = np.sqrt((p[0]-rp[0])*(p[0]-rp[0])+(p[1]-rp[1])*(p[1]-rp[1])+(p[2]-rp[2])*(p[2]-rp[2])
                      )
        if (dt < d) and (dt > 0.1):
            d = dt
            ri = i
            
    return ri
            
            
        

max_th = 0.35
P = []

scores = poses[:,3]
max_pose = poses[poses[:,3] == max(scores),:]


ax.scatter(poses[:,0] , poses[:,1] , poses[:,2],s=50,c='r',marker='+')
plt.draw()
plt.pause(5)
r_idx = np.array([1 , 54 ]) #sort(np.random.random_integers(2,50,size = 1))

#r_idx = np.random.sample(xrange(1,50), 50)

print r_idx

for i in np.arange(1,50): #np.arange(1,r_idx.shape[0]):
    plt.cla()
    ax.scatter(poses[:,0] , poses[:,1] , poses[:,2],s=5,c='r',marker='+')
    ax1.scatter(poses[:,0] , poses[:,1] , poses[:,2],s=50,c='r',marker='+')
    
    ax1.scatter(max_pose[:,0] , max_pose[:,1] , max_pose[:,2],s=150,c='b',marker='*')
    ax.scatter(max_pose[:,0] , max_pose[:,1] , max_pose[:,2],s=250,c='b',marker='*')
    
    idx_p = r_idx
    print idx_p
    
    ax.scatter(poses[idx_p,0] , poses[idx_p,1] , poses[idx_p,2],s=100,c='r',marker='o')

    p,idx,values,density = get_maxlk(idx_p,max_th)
    x, y, z = values    
    ax.scatter(x[idx], y[idx], z[idx], c = density[idx],s=10,marker='o')
    
    P.append(p)
    
    pt = np.reshape(P,(-1,3))
    
    ax1.plot_wireframe(pt[:,0],pt[:,1],pt[:,2],linewidth=2,color='y')
    
    ax1.scatter(pt[-1,0] , pt[-1,1] , pt[-1,2],s=150,c='g',marker='*')
    
    cp = p
    
    next_idx = find_closest(poses,cp)
    
    r_idx = np.append(r_idx,next_idx)
    
    #ax.plot_wireframe(pt[:,0],pt[:,1],pt[:,2],linewidth=2,color='y')
    #ax.scatter(pt[-1,0] , pt[-1,1] , pt[-1,2],s=150,c='g',marker='*')
    
    plt.pause(0.01)    
    plt.draw()
    





#ax.plot_wireframe(P1[:,0],P1[:,1],P1[:,2])




