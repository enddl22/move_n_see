#! /usr/bin/python
import roslib
import rospy
import sys
import os
import cv
import time
from threading import Thread
import numpy as np
from sensor_msgs.msg import Image
from std_msgs.msg import UInt64
from std_srvs.srv import Empty
import std_srvs.srv
from geometry_msgs.msg import PoseStamped
from scipy import stats
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
#import message_filters

class DataExtractor(object):
    def __init__(self):
        self.poses = []
        self.scores = []
        rospy.init_node('listener_pose')
        rospy.Subscriber("/vrep/LeftEndEffectorPose", PoseStamped, self.callback_pose)
        rospy.Subscriber("/MATLAB/detector", UInt64, self.callback_score)
        s = rospy.Service('save_data', Empty, self.save_data)
        #self.pose_sub = message_filters.Subscriber("/vrep/LeftEndEffectorPose",PoseStamped)
        #self.score_sub = message_filters.Subscriber("/MATLAB/detector",UInt64)
        #self.ts = message_filters.TimeSynchronizer([self.pose_sub, self.score_sub], 10)
        #self.ts.registerCallback(self.callback_pose_score)
        
    def callback_pose(self,data):
        self.poses.append([data.pose.position.x,data.pose.position.y,data.pose.position.z])
        
    
    def callback_score(self,data):
        self.scores.append(data.data)
        
    
    def callback_pose_score(self,pose,score):
        print score
    
    def save_data(self,req):    
        data = np.zeros((len(self.poses),4))            
        i = 0
        for p in self.poses:
            data[i,:] = [p[0],p[1],p[2],self.scores[i]]
            i+=1
        print data
        np.save('pick',data)
        return std_srvs.srv.EmptyResponse()
        
    def plot_poses(self):
        fig, ax = plt.subplots(subplot_kw=dict(projection='3d'))
        for p in self.poses:
            mu=np.array([p[0] , p[1] , p[2]])
            sigma=np.matrix([[4,10,0],[10,25,0],[0,0,100]])
            data=np.random.multivariate_normal(mu,sigma,100)
            values = data.T
            x, y, z = values
            ax.scatter(x, y, z)
        plt.show()
            
    
    def run(self):
        rospy.spin()


def main(args):    
    de = DataExtractor()
    de.run()   
    
if __name__ == '__main__':
    main(sys.argv)    
        
        
#import numpy as np
#from scipy import stats
#import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
#
#mu=np.array([1,10,20])
#sigma=np.matrix([[4,10,0],[10,25,0],[0,0,100]])
#data=np.random.multivariate_normal(mu,sigma,1000)
#values = data.T
#
#kde = stats.gaussian_kde(values)
#density = kde(values)
#
#fig, ax = plt.subplots(subplot_kw=dict(projection='3d'))
#x, y, z = values
#ax.scatter(x, y, z, c=density)
#plt.show()